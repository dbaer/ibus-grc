# -*- coding: utf-8 -*-
# vim:set noet ts=4:
#
# ibus-grc - The Input Bus polytonic Greek input method
#
# Copyright (c) 2011 David Baer <david.a.baer@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

from greek_alphabet import *

ACCENT_ACUTE      = 1
ACCENT_GRAVE      = 2
ACCENT_CIRCUMFLEX = 4
IOTA_SUBSCRIPT    = 8
BREATHING_ROUGH   = 16
BREATHING_SMOOTH  = 32
LENGTH_MACRON = 64
LENGTH_VRACHY = 128
DIALYTIKA     = 256
DIPTHONG      = 512

MASK_ACCENTS = ACCENT_ACUTE|ACCENT_GRAVE|ACCENT_CIRCUMFLEX
MASK_BREATHING = BREATHING_ROUGH|BREATHING_SMOOTH

a = 5

_decomposition_table = {
    u"\u1f00" : (alpha, BREATHING_SMOOTH),
    u"\u1f01" : (alpha, BREATHING_ROUGH),
    u"\u1f02" : (alpha, BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f03" : (alpha, BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f04" : (alpha, BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f05" : (alpha, BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f06" : (alpha, BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f07" : (alpha, BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1f08" : (Alpha, BREATHING_SMOOTH),
    u"\u1f09" : (Alpha, BREATHING_ROUGH),
    u"\u1f0a" : (Alpha, BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f0b" : (Alpha, BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f0c" : (Alpha, BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f0d" : (Alpha, BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f0e" : (Alpha, BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f0f" : (Alpha, BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1f10" : (epsilon, BREATHING_SMOOTH),
    u"\u1f11" : (epsilon, BREATHING_ROUGH),
    u"\u1f12" : (epsilon, BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f13" : (epsilon, BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f14" : (epsilon, BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f15" : (epsilon, BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f18" : (Epsilon, BREATHING_SMOOTH),
    u"\u1f19" : (Epsilon, BREATHING_ROUGH),
    u"\u1f1a" : (Epsilon, BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f1b" : (Epsilon, BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f1c" : (Epsilon, BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f1d" : (Epsilon, BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f20" : (eta, BREATHING_SMOOTH),
    u"\u1f21" : (eta, BREATHING_ROUGH),
    u"\u1f22" : (eta, BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f23" : (eta, BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f24" : (eta, BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f25" : (eta, BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f26" : (eta, BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f27" : (eta, BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1f28" : (Eta, BREATHING_SMOOTH),
    u"\u1f29" : (Eta, BREATHING_ROUGH),
    u"\u1f2a" : (Eta, BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f2b" : (Eta, BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f2c" : (Eta, BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f2d" : (Eta, BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f2e" : (Eta, BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f2f" : (Eta, BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1f30" : (iota, BREATHING_SMOOTH),
    u"\u1f31" : (iota, BREATHING_ROUGH),
    u"\u1f32" : (iota, BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f33" : (iota, BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f34" : (iota, BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f35" : (iota, BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f36" : (iota, BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f37" : (iota, BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1f38" : (Iota, BREATHING_SMOOTH),
    u"\u1f39" : (Iota, BREATHING_ROUGH),
    u"\u1f3a" : (Iota, BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f3b" : (Iota, BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f3c" : (Iota, BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f3d" : (Iota, BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f3e" : (Iota, BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f3f" : (Iota, BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1f40" : (omicron, BREATHING_SMOOTH),
    u"\u1f41" : (omicron, BREATHING_ROUGH),
    u"\u1f42" : (omicron, BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f43" : (omicron, BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f44" : (omicron, BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f45" : (omicron, BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f46" : (omicron, BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f47" : (omicron, BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1f48" : (Omicron, BREATHING_SMOOTH),
    u"\u1f49" : (Omicron, BREATHING_ROUGH),
    u"\u1f4a" : (Omicron, BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f4b" : (Omicron, BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f4c" : (Omicron, BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f4d" : (Omicron, BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f4e" : (Omicron, BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f4f" : (Omicron, BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1f50" : (upsilon, BREATHING_SMOOTH),
    u"\u1f51" : (upsilon, BREATHING_ROUGH),
    u"\u1f52" : (upsilon, BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f53" : (upsilon, BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f54" : (upsilon, BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f55" : (upsilon, BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f56" : (upsilon, BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f57" : (upsilon, BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1f58" : (Upsilon, BREATHING_SMOOTH),
    u"\u1f59" : (Upsilon, BREATHING_ROUGH),
    u"\u1f5a" : (Upsilon, BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f5b" : (Upsilon, BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f5c" : (Upsilon, BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f5d" : (Upsilon, BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f5e" : (Upsilon, BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f5f" : (Upsilon, BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1f60" : (omega, BREATHING_SMOOTH),
    u"\u1f61" : (omega, BREATHING_ROUGH),
    u"\u1f62" : (omega, BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f63" : (omega, BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f64" : (omega, BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f65" : (omega, BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f66" : (omega, BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f67" : (omega, BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1f68" : (Omega, BREATHING_SMOOTH),
    u"\u1f69" : (Omega, BREATHING_ROUGH),
    u"\u1f6a" : (Omega, BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f6b" : (Omega, BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f6c" : (Omega, BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f6d" : (Omega, BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f6e" : (Omega, BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f6f" : (Omega, BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1f70" : (alpha, ACCENT_GRAVE),
    u"\u1f71" : (alpha, ACCENT_ACUTE),
    u"\u1f72" : (epsilon, ACCENT_GRAVE),
    u"\u1f73" : (epsilon, ACCENT_ACUTE),
    u"\u1f74" : (eta, ACCENT_GRAVE),
    u"\u1f75" : (eta, ACCENT_ACUTE),
    u"\u1f76" : (iota, ACCENT_GRAVE),
    u"\u1f77" : (iota, ACCENT_ACUTE),
    u"\u1f78" : (omicron, ACCENT_GRAVE),
    u"\u1f79" : (omicron, ACCENT_ACUTE),
    u"\u1f7a" : (upsilon, ACCENT_GRAVE),
    u"\u1f7b" : (upsilon, ACCENT_ACUTE),
    u"\u1f7c" : (omega, ACCENT_GRAVE),
    u"\u1f7d" : (omega, ACCENT_ACUTE),

    u"\u1f80" : (alpha, IOTA_SUBSCRIPT|BREATHING_SMOOTH),
    u"\u1f81" : (alpha, IOTA_SUBSCRIPT|BREATHING_ROUGH),
    u"\u1f82" : (alpha, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f83" : (alpha, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f84" : (alpha, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f85" : (alpha, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f86" : (alpha, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f87" : (alpha, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1f88" : (Alpha, IOTA_SUBSCRIPT|BREATHING_SMOOTH),
    u"\u1f89" : (Alpha, IOTA_SUBSCRIPT|BREATHING_ROUGH),
    u"\u1f8a" : (Alpha, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f8b" : (Alpha, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f8c" : (Alpha, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f8d" : (Alpha, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f8e" : (Alpha, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f8f" : (Alpha, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1f90" : (eta, IOTA_SUBSCRIPT|BREATHING_SMOOTH),
    u"\u1f91" : (eta, IOTA_SUBSCRIPT|BREATHING_ROUGH),
    u"\u1f92" : (eta, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f93" : (eta, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f94" : (eta, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f95" : (eta, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f96" : (eta, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f97" : (eta, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1f98" : (Eta, IOTA_SUBSCRIPT|BREATHING_SMOOTH),
    u"\u1f99" : (Eta, IOTA_SUBSCRIPT|BREATHING_ROUGH),
    u"\u1f9a" : (Eta, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1f9b" : (Eta, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1f9c" : (Eta, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1f9d" : (Eta, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1f9e" : (Eta, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1f9f" : (Eta, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1fa0" : (omega, IOTA_SUBSCRIPT|BREATHING_SMOOTH),
    u"\u1fa1" : (omega, IOTA_SUBSCRIPT|BREATHING_ROUGH),
    u"\u1fa2" : (omega, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1fa3" : (omega, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1fa4" : (omega, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1fa5" : (omega, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1fa6" : (omega, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1fa7" : (omega, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_CIRCUMFLEX),
    u"\u1fa8" : (Omega, IOTA_SUBSCRIPT|BREATHING_SMOOTH),
    u"\u1fa9" : (Omega, IOTA_SUBSCRIPT|BREATHING_ROUGH),
    u"\u1faa" : (Omega, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_GRAVE),
    u"\u1fab" : (Omega, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_GRAVE),
    u"\u1fac" : (Omega, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_ACUTE),
    u"\u1fad" : (Omega, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_ACUTE),
    u"\u1fae" : (Omega, IOTA_SUBSCRIPT|BREATHING_SMOOTH|ACCENT_CIRCUMFLEX),
    u"\u1faf" : (Omega, IOTA_SUBSCRIPT|BREATHING_ROUGH|ACCENT_CIRCUMFLEX),

    u"\u1fb0" : (alpha, LENGTH_VRACHY),
    u"\u1fb1" : (alpha, LENGTH_MACRON),
    u"\u1fb2" : (alpha, IOTA_SUBSCRIPT|ACCENT_GRAVE),
    u"\u1fb3" : (alpha, IOTA_SUBSCRIPT),
    u"\u1fb4" : (alpha, IOTA_SUBSCRIPT|ACCENT_ACUTE),
    u"\u1fb6" : (alpha, ACCENT_CIRCUMFLEX),
    u"\u1fb7" : (alpha, IOTA_SUBSCRIPT|ACCENT_CIRCUMFLEX),
    u"\u1fb8" : (Alpha, LENGTH_VRACHY),
    u"\u1fb9" : (Alpha, LENGTH_MACRON),
    u"\u1fba" : (Alpha, ACCENT_GRAVE),
    u"\u1fbb" : (Alpha, ACCENT_ACUTE),
    u"\u1fbc" : (Alpha, IOTA_SUBSCRIPT),

    u"\u1fc2" : (eta, IOTA_SUBSCRIPT|ACCENT_GRAVE),
    u"\u1fc3" : (eta, IOTA_SUBSCRIPT),
    u"\u1fc4" : (eta, IOTA_SUBSCRIPT|ACCENT_ACUTE),
    u"\u1fc6" : (eta, ACCENT_CIRCUMFLEX),
    u"\u1fc7" : (eta, IOTA_SUBSCRIPT|ACCENT_CIRCUMFLEX),
    u"\u1fc8" : (Epsilon, ACCENT_GRAVE),
    u"\u1fc9" : (Epsilon, ACCENT_ACUTE),
    u"\u1fca" : (Eta, ACCENT_GRAVE),
    u"\u1fcb" : (Eta, ACCENT_ACUTE),
    u"\u1fcc" : (Eta, IOTA_SUBSCRIPT),

    u"\u1fd0" : (iota, LENGTH_VRACHY),
    u"\u1fd1" : (iota, LENGTH_MACRON),
    u"\u03ca" : (iota, DIALYTIKA),
    u"\u1fd2" : (iota, DIALYTIKA|ACCENT_GRAVE),
    u"\u1fd3" : (iota, DIALYTIKA|ACCENT_ACUTE),
    u"\u1fd6" : (iota, ACCENT_CIRCUMFLEX),
    u"\u1fd7" : (iota, DIALYTIKA|ACCENT_CIRCUMFLEX),
    u"\u1fd8" : (Iota, LENGTH_VRACHY),
    u"\u1fd9" : (Iota, LENGTH_MACRON),
    u"\u1fda" : (Iota, ACCENT_GRAVE),
    u"\u1fdb" : (Iota, ACCENT_ACUTE),
    u"\u03aa" : (Iota, DIALYTIKA),

    u"\u1fe0" : (upsilon, LENGTH_VRACHY),
    u"\u1fe1" : (upsilon, LENGTH_MACRON),
    u"\u03cb" : (upsilon, DIALYTIKA),
    u"\u1fe2" : (upsilon, DIALYTIKA|ACCENT_GRAVE),
    u"\u1fe3" : (upsilon, DIALYTIKA|ACCENT_ACUTE),
    u"\u1fe4" : (rho, BREATHING_SMOOTH),
    u"\u1fe5" : (rho, BREATHING_ROUGH),
    u"\u1fe6" : (upsilon, ACCENT_CIRCUMFLEX),
    u"\u1fe7" : (upsilon, DIALYTIKA|ACCENT_CIRCUMFLEX),
    u"\u1fe8" : (Upsilon, LENGTH_VRACHY),
    u"\u1fe9" : (Upsilon, LENGTH_MACRON),
    u"\u1fea" : (Upsilon, ACCENT_GRAVE),
    u"\u1feb" : (Upsilon, ACCENT_ACUTE),
    u"\u03ab" : (Upsilon, DIALYTIKA),
    u"\u1fec" : (Rho, BREATHING_ROUGH),

    u"\u1ff2" : (omega, IOTA_SUBSCRIPT|ACCENT_GRAVE),
    u"\u1ff3" : (omega, IOTA_SUBSCRIPT),
    u"\u1ff4" : (omega, IOTA_SUBSCRIPT|ACCENT_ACUTE),
    u"\u1ff6" : (omega, ACCENT_CIRCUMFLEX),
    u"\u1ff7" : (omega, IOTA_SUBSCRIPT|ACCENT_CIRCUMFLEX),
    u"\u1ff8" : (Omicron, ACCENT_GRAVE),
    u"\u1ff9" : (Omicron, ACCENT_ACUTE),
    u"\u1ffa" : (Omega, ACCENT_GRAVE),
    u"\u1ffb" : (Omega, ACCENT_ACUTE),
    u"\u1ffc" : (Omega, IOTA_SUBSCRIPT) }

def decompose_character(ch):
    "decompose_character(ch) -> ( stripped_character, diacritics )"
    if _decomposition_table.has_key(ch):
        return _decomposition_table[ch]
    else:
        return (ch, 0)
    
_composition_table = dict([ (_decomposition_table[k], k) for k in _decomposition_table.keys() ])

def compose_character(vowel, diacritics):
    global _composition_table
    if diacritics == 0:
        return vowel
    else:
        return _composition_table[(vowel, diacritics)]

