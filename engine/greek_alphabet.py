# vim:set et sts=4 sw=4:
#
# ibus-grc - The Input Bus polytonic Greek input method
#
# Copyright (c) 2011 David Baer <david.a.baer@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

alpha   = u"\u03b1"
beta    = u"\u03b2"
gamma   = u"\u03b3"
delta   = u"\u03b4"
epsilon = u"\u03b5"
zeta    = u"\u03b6"
eta     = u"\u03b7"
theta   = u"\u03b8"
iota    = u"\u03b9"
kappa   = u"\u03ba"
lambda_ = u"\u03bb"
mu      = u"\u03bc"
nu      = u"\u03bd"
xi      = u"\u03be"
omicron = u"\u03bf"
pi      = u"\u03c0"
rho     = u"\u03c1"
sigma_final = u"\u03c2"
sigma   = u"\u03c3"
tau     = u"\u03c4"
upsilon = u"\u03c5"
phi     = u"\u03c6"
chi     = u"\u03c7"
psi     = u"\u03c8"
omega   = u"\u03c9"

Alpha   = u"\u0391"
Beta    = u"\u0392"
Gamma   = u"\u0393"
Delta   = u"\u0394"
Epsilon = u"\u0395"
Zeta    = u"\u0396"
Eta     = u"\u0397"
Theta   = u"\u0398"
Iota    = u"\u0399"
Kappa   = u"\u039a"
Lambda  = u"\u039b"
Mu      = u"\u039c"
Nu      = u"\u039d"
Xi      = u"\u039e"
Omicron = u"\u039f"
Pi      = u"\u03a0"
Rho     = u"\u03a1"
Sigma   = u"\u03a3"
Tau     = u"\u03a4"
Upsilon = u"\u03a5"
Phi     = u"\u03a6"
Chi     = u"\u03a7"
Psi     = u"\u03a8"
Omega   = u"\u03a9"

def isvowel(ch):
    return ch == alpha or ch == Alpha or ch == epsilon or ch == Epsilon or \
           ch == eta or ch == Eta or ch == iota or ch == Iota or \
           ch == omicron or ch == Omicron or ch == upsilon or ch == Upsilon or \
           ch == omega or ch == Omega

def isconsonant(ch):
    # note: because of tolower call, result is undefined for accented vowels
    #       (it's best to strip the diacritics before calling this)
    return tolower(ch) in [ beta, gamma, delta, zeta, theta, kappa, lambda_,
                              mu, nu, xi, pi, rho, sigma, tau, phi, chi, psi,
                              omega ]

def tolower(word):
    result = ""
    for ch in word:
        code = ord(ch)
        if 0x0391 <= code <= 0x03a9:
            result += unichr(code + 0x20)
        else:
            result += ch
    return result

def isdipthong(comb):
    return tolower(comb) in [ alpha + iota, alpha + upsilon, epsilon + iota,
                              epsilon + upsilon, epsilon + omega, eta + upsilon,
                              omicron + iota, omicron + upsilon, upsilon + iota ]

