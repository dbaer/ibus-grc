# vim:set et sts=4 sw=4:
#
# ibus-grc - The Input Bus polytonic Greek input method
#
# Copyright (c) 2011-2014 David Baer <david.a.baer@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# for python2
from __future__ import print_function, unicode_literals

import sys
if sys.version_info.major < 3:
    chr = unichr
elif sys.version_info.major >= 3:
    from functools import reduce

import gi
gi.require_version('Pango', '1.0')
from gi.repository import GLib
from gi.repository import IBus
from gi.repository import Pango
from accents import *
from greek_alphabet import *

keysyms = IBus

class EngineGRC(IBus.Engine):
    __gtype_name__ = 'EngineGRC'

    __greek_finals = {
        'b' : '\u03b2', 'B' : '\u0392',
        'g' : '\u03b3', 'G' : '\u0393',
        'd' : '\u03b4', 'D' : '\u0394',
        'z' : '\u03b6', 'Z' : '\u0396',
        'q' : '\u03b8', 'Q' : '\u0398',
        'k' : '\u03ba', 'K' : '\u039a',
        'l' : '\u03bb', 'L' : '\u039b',
        'm' : '\u03bc', 'M' : '\u039c',
        'n' : '\u03bd', 'N' : '\u039d',
        'c' : '\u03be', 'C' : '\u039e',
        'p' : '\u03c0', 'P' : '\u03a0',
        'j' : '\u03c2', 'S' : '\u03a3',
        't' : '\u03c4', 'T' : '\u03a4',
        'f' : '\u03c6', 'F' : '\u03a6',
        'x' : '\u03c7', 'X' : '\u03a7',
        'y' : '\u03c8', 'Y' : '\u03a8',
    }

    __greek_alphabet = {
        'a' : '\u03b1', 'A' : '\u0391',
        'e' : '\u03b5', 'E' : '\u0395',
        'h' : '\u03b7', 'H' : '\u0397',
        'i' : '\u03b9', 'I' : '\u0399',
        'o' : '\u03bf', 'O' : '\u039f',
        'r' : '\u03c1', 'R' : '\u03a1',
        's' : '\u03c2',
        'u' : '\u03c5', 'U' : '\u03a5',
        'w' : '\u03c9', 'W' : '\u03a9',
    }

    __greek_accents = {
            '/' : ACCENT_ACUTE,
            '\\' : ACCENT_GRAVE,
            '=' : ACCENT_CIRCUMFLEX,
            ')' : BREATHING_SMOOTH,
            '(' : BREATHING_ROUGH,
            '|' : IOTA_SUBSCRIPT,
            '"' : DIALYTIKA,
            '-' : LENGTH_VRACHY,
            '_' : LENGTH_MACRON,
    }

    def __init__(self):
        super(EngineGRC, self).__init__()
        self.__is_invalidate = False
        self.__preedit_string = ""
        #self.__lookup_table = IBus.LookupTable.new(10, 0, True, True)
        self.__prop_list = IBus.PropList()
        self.__prop_list.append(IBus.Property(key="test", icon="ibus-local"))
        print("Create EngineGRC OK")

    def do_process_key_event(self, keyval, keycode, state):
        #print("process_key_event(%04x, %04x, %04x)" % (keyval, keycode, state))
        # ignore key release events
        is_press = ((state & IBus.ModifierType.RELEASE_MASK) == 0)
        if not is_press:
            return False

        if self.__preedit_string:
            if keyval == keysyms.Return:
                self.__commit_string(self.__preedit_string)
                return True
            elif keyval == keysyms.Escape:
                self.__preedit_string = ""
                self.__update()
                return True
            elif keyval == keysyms.BackSpace:
                if self.__symbol_stack:
                    self.__symbol_stack = self.__symbol_stack[:-1]
                    if self.__symbol_stack:
                        ch = self.__symbol_stack[0]
                        dia = reduce(lambda x,y:x|y, self.__symbol_stack[1:],
                                     0)
                        self.__preedit_string = self.__preedit_string[:-1] + \
                            compose_character(ch, dia)
                        self.__update()
                    else:
                        self.__preedit_string = ""
                        self.__update()
                else:
                    self.__preedit_string = self.__preedit_string[:-1]
                    self.__invalidate()
                return True
            elif keyval == keysyms.space:
                self.__commit_string(self.__preedit_string + ' ')
                return True
            elif keyval == keysyms.Left or keyval == keysyms.Right:
                return True
        if keyval in [ keysyms.colon, keysyms.semicolon ]:
            self.__commit_string(self.__preedit_string + '\u0387')
            return True
        elif keyval == keysyms.question:
            self.__commit_string(self.__preedit_string + '\u037e')
            return True
        if keyval in range(keysyms.a, keysyms.z + 1) or \
            keyval in range(keysyms.A, keysyms.Z + 1):
            if state & (IBus.ModifierType.CONTROL_MASK | \
                        IBus.ModifierType.MOD1_MASK) == 0:
                chrval = chr(keyval)
                if chrval in self.__greek_finals:
                    if self.__preedit_string == '\u03c2':
                        self.__commit_string('\u03c3')
                    self.__preedit_string += self.__greek_finals[chrval]
                    self.__commit_string(self.__preedit_string)
                    return True
                elif chrval in self.__greek_alphabet:
                    if self.__preedit_string == '\u03c2':
                        self.__commit_string('\u03c3')
                    else:
                        self.__commit_string(self.__preedit_string)
                    greek_char = self.__greek_alphabet[chrval]
                    self.__preedit_string += greek_char
                    self.__symbol_stack.append(greek_char)
                    self.__update()
                    return True
                else:
                    self.__preedit_string += chr(keyval)
                    self.__invalidate()
                return True
        elif chr(keyval) in self.__greek_accents and \
            self.__symbol_stack:
            ch = self.__symbol_stack[0]
            if isvowel(ch) or ch in ( rho, Rho ):
                greek_accent = self.__greek_accents[chr(keyval)]
                self.__symbol_stack.append(greek_accent)
                dia = reduce(lambda x,y:x|y, self.__symbol_stack[1:], 0)
                self.__preedit_string = self.__preedit_string[:-1] + \
                    compose_character(ch, dia)
                self.__invalidate()
                return True
            else:
                self.__commit_string(self.__preedit_string)
        else:
            if keyval < 128 and self.__preedit_string:
                self.__commit_string(self.__preedit_string)

        return False

    def __invalidate(self):
        if self.__is_invalidate:
            return
        self.__is_invalidate = True
        GLib.idle_add(self.__update)
        self.hide_lookup_table()

    def __commit_string(self, text):
        self.commit_text(IBus.Text.new_from_string(text))
        self.__preedit_string = ''
        self.__symbol_stack = [ ]
        self.__update()

    def __update(self):
        preedit_len = len(self.__preedit_string)
        attrs = IBus.AttrList()
        if preedit_len > 0:
            attrs.append(
                IBus.AttrUnderline.new(
                    IBus.AttrUnderline.SINGLE, 0, preedit_len
                )
            )
        text = IBus.Text.new_from_string(self.__preedit_string)
        text.set_attributes(attrs)
        self.update_preedit_text(text, preedit_len, preedit_len > 0)
        self.__is_invalidate = False

    def show_lookup_table(self):
        return True

    def do_focus_in(self):
        #print("focus_in")
        self.register_properties(self.__prop_list)

    def do_focus_out(self):
        #print("focus_out")
        pass

    def do_reset(self):
        #print("reset")
        pass

    def do_property_activate(self, prop_name):
        #print("PropertyActivate(%s)" % prop_name)
        pass

